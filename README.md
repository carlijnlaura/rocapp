<h1 align="center">
  RocApp
</h1>

<h4 align="center">A Fully Featured Student Progress and Internship Website Built With <a href="https://laravel.com/" target="_blank">Laravel</a> for Roc.</h4>

## About RocApp

The RocApp is a web application that every school would want. We realised an application where its possible to manage internships of students, in a user friendly way. RocApp has a lovable UI that you won't find elsewhere, and functions so easy to understand, even the most basic user wont have trouble using RocApp.

## Key Features

- Internship hours managment for students and over students (in progress)
- A dynamic requirementlist (in progress)
- Internship managment
- Company managment
- Student managment (includes managment over kwalification files)
- Teacher panel
- Direct notifications to teachers
- Custom API supported Google maps Iframe for company's
- Profile manager

## Setup

To clone and setup this application, you'll need [Git](https://git-scm.com), [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) and [Composer](https://getcomposer.org/) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://ictwf.visualstudio.com/2020_ADSD_Project_Frameworks_TeamB1/_git/2020_ADSD_Project_Frameworks_TeamB1

# Go into the repository
$ cd 2020_ADSD_Project_Frameworks_TeamB1

# Install dependencies
$ npm install
$ composer i

# Build assets
$ npm run dev
```

## Migrations

Setup your database with a new table and update these credentials in the .env file
```
DB_DATABASE=rocapp #Your table name
DB_USERNAME=root #Your database username
DB_PASSWORD=root #Your database password
```

After this you can run the migrations command to setup the tabel with correct fields and data

```bash
# Run migrations
$ php artisan migrate --seed
```

## Run the app

After you have succesfully did the previous tasks you can now run the app
```bash
# Run the app
$ php artisan serve
```


## Definition of done

1. Code tested
2. Code reviewed
3. Merge conflicts resolved
4. Merge request reviewed by atleast 1 developer not connected with the changes
4. Merged in master branch
5. Product owner accepted User Story

## Credits

- Samir Mokiem - s1158232@student.windesheim.nl
- Safouane Messaoudi - s1158048@student.windesheim.nl
- Maksim Hofker - s1161427@student.windesheim.nl
- Carlijn den Hartog - s1158298@student.windesheim.nl
- Stijn Engelmoer - s1157591@student.windesheim.nl
