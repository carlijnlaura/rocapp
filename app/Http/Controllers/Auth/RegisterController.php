<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    protected function validator(array $data)
    {
        if ($data['type'] === 'student') {
            return Validator::make($data, [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        }
        return Validator::make($data, [
            'company_name' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'string', 'max:255'],
            'website' => ['required', 'string', 'max:255'],
            'kvk' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'street' => ['required', 'string'],
            'place' => ['required', 'string'],
            'zipcode' => ['required', 'string'],
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'] ?? '',
            'last_name' => $data['last_name'] ?? '',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'type' => $data['type'] === 'bpv' ? 'bpv' : 'student',
            'company_name' => $data['company_name'] ?? '',
            'phone_number' => $data['phone_number'] ?? '',
            'website' => $data['website'] ?? '',
            'kvk' => $data['kvk'] ?? '',
            'street' => $data['street'] ?? '',
            'place' => $data['place'] ?? '',
            'zipcode' => $data['zipcode'] ?? '',
        ]);
    }
}
