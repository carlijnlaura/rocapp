<?php

namespace App\Http\Controllers;

use App\Models\BPV;
use App\Models\User;
use Illuminate\Http\Request;

class CompanyController
{

    public function index(Request $request)
    {
        if ($request->has('q')) {
            $companies = BPV::where('company_name', 'LIKE', "%" . $request->get('q') . "%")->paginate(30);
        } else {
            $companies = BPV::paginate(30);
        }
        return view('company.index', compact('companies'));
    }

    public function show(User $company)
    {
        return view('company.show', compact('company'));
    }

}
