<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get($file)
    {
        return Storage::disk('private')->get($file);
    }

 }