<?php

namespace App\Http\Controllers;

use App\Models\Hour;
use App\Models\Internship;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->type == 'bpv') {
            $hour = Hour::all();
            $users = User::where('type','student')->get();
            return view('hours/bpv-hourslist', compact('hour'), compact('users'));
        }
        if ($user->type == 'student') {
            $id = $user->id;
            $hour = Hour::where('userid',$id)->get();
            return view('hours/student-hourslist', compact('hour'));
        }
        if ($user->type == 'teacher') {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        if ($user->type == 'student') {
            $id = $user->id;
            $hour = new Hour;

            $hour->intern_id = $request->intern_id;
            $hour->hours = $request->uren;
            $hour->date = $request->datum;
            $hour->begintime = $request->begintijd;
            $hour->endtime = $request->eindtijd;
            $hour->userid = $id;

            $hour->save();
            return redirect(route('hours.index'));
        }
        if ($user->type == 'bpv') {
            $hour = new Hour;

            $companyid = $user->id;
            $userid = $request->user_name;


            $internship = Internship::where('company_id', $companyid)->where('user_id', $userid)->first();

            $hour->intern_id = $internship->id;
            $hour->hours = $request->uren;
            $hour->date = $request->datum;
            $hour->begintime = $request->begintijd;
            $hour->endtime = $request->eindtijd;

            $hour->userid = $userid;

            $hour->save();

            return redirect(route('hours.index'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        if ($user->type == 'student') {
            $users = User::where('type','bpv')->get();
            return view('hours/create-hours', compact('users'));
        }
        if ($user->type == 'bpv') {
            $users = User::where('type','student')->get();
            return view('hours/create-hours-bpv', compact('users'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function edit(Hour $hour, $id)
    {
        $user = Auth::user();
        $hour = Hour::where('id',$id)->get();
        $users = User::where('type','bpv')->get();
        if ($user->type == 'student') return view('hours/change-hours', compact('hour'), compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hour $hour, $id)
    {
        $user = Auth::user();
        $userid = $user->id;

        $hour = Hour::find($id);

        $hour->intern_id = $request->intern_id;
        $hour->hours = $request->uren;
        $hour->date = $request->datum;
        $hour->begintime = $request->begintijd;
        $hour->endtime = $request->eindtijd;
        $hour->userid = $userid;

        $hour->save();

        return redirect(route('hours.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hour $hour, $id)
    {
        $hour = Hour::find($id);
        $hour->delete();
        return redirect(route('hours.index'));
    }

    /**
     * deny
     *
     * @return \Illuminate\Http\Response
     */
    public function deny($id)
    {
        $hour = Hour::find($id);
        $hour->status = 'Denied';
        $hour->save();
        return redirect(route('hours.index', compact('id')));
    }
}
