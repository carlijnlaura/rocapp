<?php

namespace App\Http\Controllers;

use App\Models\Internship;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class InternshipController extends Controller
{

    //Todo:: relation

    public function request(User $company)
    {
        return view('internship.request', compact('company'));
    }

    public function studentsRequests()
    {
        $internships = Internship::where('user_id', '=', auth()->user()->id)->paginate(30);
        return view('internship.student_requests', compact('internships'));
    }

    public function requestPost(Request $request, User $company)
    {
        $request->validate(Internship::$rules);
        Internship::create([
            'user_id' => auth()->user()->id,
            'company_id' => $company->id,
            'start_date' => date('Y-m-d', strtotime($request->post('start_date'))),
            'function' => $request->post('function'),
            'end_date' => date('Y-m-d', strtotime($request->post('end_date'))),
            'hours' => $request->post('hours')
        ]);
        return redirect('/company/requests');
    }

    public function requests(Request $request)
    {
        if($request->has('d')) {
            $internships = Internship::where([
                ['company_id', '=', auth()->user()->id],
                ['status', '=', 'denied']
            ])->paginate(30);
        } else {
            $internships = Internship::where([
                ['company_id', '=', auth()->user()->id],
                ['status', '=', Internship::$defaultStatus]
            ])->paginate(30);
        }
        return view('internship.requests', compact('internships'));
    }

    public function stagaires(Request $request)
    {
        if($request->has('f')) {
            $internships = Internship::where('company_id', auth()->user()->id)
                ->where('status', 'fired')
                ->paginate(30);
        } else {
            $internships = Internship::where('company_id', auth()->user()->id)
                ->where('status', 'accepted')
                ->orWhere('status', 'complete')
                ->paginate(30);
        }
        return view('internship.stagaires', compact('internships'));
    }

    public function acceptRequest(Internship $internship)
    {
        $internship->accept();
        return redirect('/my-company/stagaires');
    }

    public function denyRequest(Internship $internship)
    {
        $internship->deny();
        return back();
    }

    public function edit(Internship $internship)
    {
        return view('internship.edit', compact('internship'));
    }

    public function update(Request $request, Internship $internship)
    {
        $request->validate(Internship::$rules);
        $internship->update([
            'start_date' => date('Y-m-d', strtotime($request->post('start_date'))),
            'function' => $request->post('function'),
            'end_date' => date('Y-m-d', strtotime($request->post('end_date'))),
            'status' => $request->post('status'),
            'hours' => $request->post('hours')
        ]);
        return redirect('/my-company/stagaires');
    }

}
