<?php

namespace App\Http\Controllers;

use App\Models\Hour;
use App\Models\internship_hours;
use Illuminate\Http\Request;

class InternshiphoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $internship_hours = new internship_hours();
        $hour = Hour::find($id);
        $hour->status = 'Accepted';
        $hour->save();

        $internship_hours->userid = $hour->userid;
        $internship_hours->hours = $hour->hours;
        $internship_hours->date = $hour->date;
        $internship_hours->begintime = $hour->begintime;
        $internship_hours->endtime = $hour->endtime;
        $internship_hours->intern_id = $hour->intern_id;

        $internship_hours->save();
        return redirect(route('hours.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\internship_hours  $internshiphours
     * @return \Illuminate\Http\Response
     */
    public function show(internship_hours $internshiphours)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\internship_hours  $internshiphours
     * @return \Illuminate\Http\Response
     */
    public function edit(internship_hours $internshiphours)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\internship_hours  $internshiphours
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, internship_hours $internshiphours)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\internship_hours  $internshiphours
     * @return \Illuminate\Http\Response
     */
    public function destroy(internship_hours $internshiphours)
    {
        //
    }
}
