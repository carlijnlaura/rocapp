<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Training;
use App\Models\Kwalificatiedossier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class KDController extends Controller
{
    public function index()
    {
        
        $dossiers = Kwalificatiedossier::select('first_name', 'last_name', 'kwalificatiedossiers.id')
        ->join('users', 'users.id', '=', 'kwalificatiedossiers.user_id')->get();
        return view('KD.index' , compact('dossiers'));
        
        
    }
    public function showStudentKD()
    {
        $user = Auth::user();
        $KD = Kwalificatiedossier::where('user_id', $user->id)->first();
        $filename = $KD->filename;
        $file = storage_path('app/private/') . $filename;
        
        //  return view('KD.showStudentKD', compact('KD'));
         return response()->file($file);

       
    }

    public function show(Kwalificatiedossier $KD)
    {

        //maakt de url naar de file
        $training = Training::where('id', $KD->training_id)->first();
        $user = User::where('id', $KD->user_id)->first();
        $filename = ($user->first_name . $user->last_name . $KD->training_id . "-KD.pdf");
        $file = storage_path('app/private/') . $filename;
       
        return response()->file($file); // dit werkt. 

    }
    public function create()
    {

        $users = User::where('type','student')->paginate(500);
        $trainingen = Training::paginate(500);
        return view('KD.create', compact('users', 'trainingen'));
    }
    public function edit(Kwalificatiedossier $KD)
    {
        $users = User::where('type','student')->paginate(500);
        $usercurrent = User::where('id', $KD->user_id)->first();
        $trainingen = Training::paginate(500);
        $trainingcurrent = Training::where('id', $KD->training_id)->first();
        return view('KD.edit', compact('KD','usercurrent','users', 'trainingen', 'trainingcurrent'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'dossier' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);
  
         $user = User::where('id', $request->get('user_id'))->first();
        
         // filename format is = "first_nameLast_name-training_id-KD.pdf"
        $filename = ($user->first_name . $user->last_name . $request->post('training_id') . "-KD.pdf");
    
        $Kd = Kwalificatiedossier::create([
            'training_id' => $request->post('training_id'),
            'user_id' => $request->post('user_id'),
            'filename' => $filename
         
            
        ]);
       
        $request->dossier->storeAs('private', $filename);
     
        return redirect('KD');
    }
    public function update(Request $request, Kwalificatiedossier $KD)
    {
        $request->validate([
            'dossier' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);
        //delete old file
        $filepath = $KD->filename;
        Storage::disk('private')->delete($filepath);
        //create new file
        $user = User::where('id', $request->get('user_id'))->first();
        $filename = ($user->first_name . $user->last_name . $request->post('training_id') . "-KD.pdf");
        $request->dossier->storeAs('private', $filename);
        $KD->update([
            'training_id' => $request->post('training_id'),
            'user_id' => $request->post('user_id'),
            'filename' => $filename

            
        ]);
      
        return redirect(route('KD.index'));
    }
    
    public function destroy(Kwalificatiedossier $KD)
    {
        
        $filepath = $KD->filename;
        Storage::disk('private')->delete($filepath);
        $KD->delete();
        return redirect('KD');
    }
}
