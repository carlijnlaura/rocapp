<?php

namespace App\Http\Controllers;

use App\Mail\NewUserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use App\Models\TrainingUser;
use App\Models\User;

class MailController extends Controller
{
    public function store(Request $request,User $user)
    {
//dd(TrainingUser::where('training_id', $request['training'])
//    ->where('user_role', '=', 3)
//    ->get());
        $training = Training::find($request['training']);
        $users = $training->teachers;
        //dd($users);

        foreach($users as $user){
            // mail sturen per user
            Mail::to( $user->email)
                ->send(new NewUserNotification($request, $training));
    }


        return redirect()->action([TrainingController::class, 'index']);

    }
}
