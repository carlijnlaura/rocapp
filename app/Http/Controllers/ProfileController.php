<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class ProfileController
{

    public function show()
    {
        $user = Auth()->user();
        if ($user->isRole('bpv')) {
            return view('profile.bpv', compact('user'));
        }
        return view('profile.student', compact('user'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $id = $user->id;
        if ($user->isRole('bpv')) {
            auth()->user()->update($request->validate([
                'company_name' => 'required|string|max:255',
                'phone_number' => 'required|string|max:255',
                'website' => "required|string|max:255",
                'kvk' => "required|string|max:255",
                'email' => "required|string|max:255|unique:users,email,$id",
            ]));
        } else {
            auth()->user()->update($request->validate([
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => "required|string|max:255|unique:users,email,$id",
            ]));
        }
        return back();
    }

    public function updateAdres(Request $request) {
        auth()->user()->update($request->validate([
            'street' => "required|string|max:255",
            'zipcode' => "required|string|max:255",
            'place' => "required|string|max:255",
        ]));
        return back();
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string',
            'new_password' => 'required|string',
            'new_password_confirmation' => 'required|string|same:new_password',
        ]);
        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#Security")
                ->withErrors($validator);
        }
        if (!Hash::check($request->post('old_password'), auth()->user()->password)) {
            return Redirect::to(URL::previous() . "#Security")->withErrors([
                'old_password' => 'Wachtwoord komt niet overeen met huidig wachtwoord'
            ]);
        }
        $user = Auth()->user();
        $user->password = bcrypt($request->post('new_password'));
        $user->save();
        //no error but ignore pls
        return Redirect::to(URL::previous() . "#Security")->withErrors([
            'message' => 'Wachtwoord is geupdate'
        ]);
    }

    public function updateAvatar(Request $request)
    {
        $file = $request->file('file');
        Storage::putFileAs('public/avatars', $file, Auth()->user()->id . '.png');
    }

}
