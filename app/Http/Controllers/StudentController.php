<?php

namespace App\Http\Controllers;

use App\Models\Training;
use App\Models\TrainingUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('q')) {
            $users = User::where('type', 'student')->where('first_name', 'LIKE', "%" . $request->get('q') . "%")->orWhere('last_name', 'LIKE', "%" . $request->get('q') . "%")->paginate(10);
        } else {
            $users = User::where('type', 'student')->paginate(10);
        }
        return view('students.index', compact('users'));
    }

    public function show(User $student)
    {
        $trainings = Training::get();
        return view('students.show', compact('student','trainings'));
    }

    public function edit(User $student)
    {
        return view('students.edit', compact('student'));
    }

    public function update(Request $request, User $student)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required|string'
        ]);
        if ($validator->fails()) {
            return Redirect::to(URL::previous())
                ->withErrors($validator);
        }

        $student->update([
            'first_name' => $request->post('first_name'),
            'last_name' => $request->post('last_name'),
            'email' => $request->post('email'),
            'phone_number' => $request->post('phone_number')
        ]);

        return redirect()->route('students.index');
    }

    public function create()
    {
        return view('students.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'phone_number' => 'required|string',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return Redirect::to(URL::previous())
                ->withErrors($validator);
        }

        $student = User::create([
            'first_name' => $request->post('first_name'),
            'last_name' => $request->post('last_name'),
            'email' => $request->post('email'),
            'type' => 'student',
            'company_name' => '',
            'phone_number' => $request->post('phone_number'),
            'website' => '',
            'kvk' => '',
            'password' => bcrypt($request->post('password'))
        ]);

        return redirect('/students/' . $student->id)->withErrors([
            'message' => 'Student aangemaakt'
        ]);
    }

    public function delete(User $student)
    {
        $student->delete();

        return redirect()->back()->with([
            'message' => 'Student verwijderd'
        ]);
    }
}

