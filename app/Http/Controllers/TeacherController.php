<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{

    /**
     * Create new company
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createcompany(Request $request)
    {
        $user = new User;

        $user->first_name = '';
        $user->last_name = '';
        $user->type = 'bpv';
        $user->company_name = $request->company_name;
        $user->phone_number = $request->phone_number;
        $user->website = $request->website;
        $user->kvk = $request->kvk;
        $user->email = $request->email;
        $user->street = $request->street;
        $user->zipcode = $request->zipcode;
        $user->place = $request->place;

        $user->password = Hash::make($request->get('password'));

        $user->save();

        return redirect('/school-company');
    }

    public function deletecompany($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/school-company');
    }

    /**
     * Change company info
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changecompany(Request $request, $id)
    {
        $user = User::find($id);

        $user->first_name = '';
        $user->last_name = '';
        $user->type = 'bpv';
        $user->company_name = $request->company_name;
        $user->phone_number = $request->phone_number;
        $user->website = $request->website;
        $user->kvk = $request->kvk;
        $user->email = $request->email;
        $user->street = $request->street;
        $user->zipcode = $request->zipcode;
        $user->place = $request->place;

        $user->password = Hash::make($request->get('password'));

        $user->save();

        return redirect('/school-company');
    }

    public function changecompanypage()
    {
        $users = User::where('type', 'bpv')->get();
        $user = Auth::user();
        if ($user->type == 'teacher') return view('company/change-company', compact('users'));
    }

    public function createcompanypage()
    {
        $user = Auth::user();
        if ($user->type == 'teacher') return view('company/create-company');
    }

}
