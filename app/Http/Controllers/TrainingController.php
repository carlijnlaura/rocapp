<?php

namespace App\Http\Controllers;

use App\Models\Training;
use App\Models\TrainingUser;
use App\Models\User;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $trainings = Training::where([
            ['title', '!=', null],
            [function ($query) use ($request) {
            if (($title = $request->title)) {
                $query->orWhere('title', 'LIKE', '%' . $title . '%')->get();
            }
            }]
        ])
            ->paginate(10);
        return view('trainings.index', compact('trainings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Training $training)
    {
        return view('trainings.create',compact('training'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Training $training)
    {
       $training = $training->create($request->validate(Training::$rules));

        TrainingUser::create( //connect training  to user that pressed on send button
            [
                'user_role' => 3,
                'training_id' => $training->id,
                'user_id' => auth()->user()->id
            ]);

        return redirect(route('trainings.index', [$training->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function show(Training $training, User $user)
    {
        return view('trainings.show', compact('training','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Training $training)
    {
        return view('trainings.edit',compact('training'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Training $training)
    {
        $training->update($request->all());
        return redirect(route('trainings.index', [$training->id]));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function destroy(Training $training)
    {
        $training->delete();

        return redirect(route('trainings.index'));
    }
}
