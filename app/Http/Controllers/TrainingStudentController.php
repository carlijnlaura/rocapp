<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Training;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\TrainingUser;

class TrainingStudentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($training)
    {
        //todo Do not show training users but do show users without relation
//        User::student()
//            ->whereDoesntHave('groups')
//            ->get();
        //        $users = $training->student->whereDoesntHave('training');

        $users = collect();
        $temp_users = Student::all();
        $temp_users->each(function ($item) use ($training, $users) {
            if(count($item->training)) {
                foreach ($item->training as $t) {
                    if ($t->id != $training) {
                        $users->push($item);
                    }
                }
            } else {
                $users->push($item);
            }
        });
//       $users = User::leftJoin('training', 'quotes.custom_design_request_id', '=', 'custom_design_requests.id')
//        dd($users);
//        ->whereNull('quotes.custom_design_request_id')
//        ->orderBy('created_at', 'desc')->get();
        $training = Training::find($training);

        return view('trainings.student.index', compact('users', 'training'));
    }

    public function create()
    {
//        $users = User::where('type','student')->paginate(500);
//        $training = Training::all();
//        return view('trainings.student.create',compact('training','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Training $training, User $user, TrainingUser $trainingUser)
    {

        TrainingUser::create( //todo allows teacher to add a student more then 1 time to a
        // todo training thats not what we want
            [
                'user_id' => $request->post('user_id'),
                'user_role' => 1,
                'training_id' => $training->id,
            ]

        );


        return redirect('/trainings');
    }


}
