<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        $users = User::where('type', 'bpv')->get();
        $user = Auth::user();
        if ($user->type == 'teacher') return view('company/school-company', compact('users'));
        if ($user->type == 'student') return view('company/student-company', compact('users'));
    }

}
