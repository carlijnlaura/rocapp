<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class NewUserNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    public $request;
    public $training;
    public function __construct($request, $training)
    {
        $this->request = $request;
        $this->training = $training;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('noreply@rocapp.com')
            ->subject('Studenten aanvraag')
            ->view('trainings/contact_tmp')
            ->with('user', Auth::user())
            ->with('training', $this->training);


        /**
         * from -> vanaf welk mailadres, bedrijfsmail
         * subject -> onderwerp mail
         * view -> mail template
         * with('request', $this->request)
         */
    }
}
