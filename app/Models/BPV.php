<?php

namespace App\Models;

class BPV extends User
{

    protected $table = 'users';

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('type', '=', 'bpv');
    }

}
