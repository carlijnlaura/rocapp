<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    use HasFactory;

    static $defaultAnswer = 'Unclear';

    static $answers = [
        0 => 'Accepted',
        1 => 'Denied',
        2 => 'Unclear'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function company()
    {
        return $this->belongsTo(User::class, 'intern_id')->where('type','=','bpv');
    }

    public function internship()
    {
        return $this->belongsTo(Internship::class, 'intern_id', 'id');
    }
}
