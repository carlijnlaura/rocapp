<?php

namespace App\Models;

class Internship extends BaseModel
{

    protected $fillable = [
        'user_id',
        'company_id',
        'start_date',
        'end_date',
        'hours',
        'function',
        'status'
    ];

    static $rules = [
        'function' => 'required|string|max:255',
        'start_date' => 'required|date',
        'end_date' => 'required|date',
        'hours' => 'required|int',
    ];

    static $defaultStatus = 'awaiting_approval';

    static $availableStatus = [
        'awaiting_approval',
        'accepted',
        'denied',
        'complete',
        'fired'
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Company()
    {
        return $this->belongsTo(User::class, 'company_id', 'id');
    }

    public function Internships()
    {
        return $this->hasMany(Internship::class);
    }

    public function accept() {
        $this->update([
            'status' => 'accepted'
        ]);
    }

    public function deny() {
        $this->update([
            'status' => 'denied'
        ]);
    }

    public function fire()
    {
        $this->update([
            'status' => 'fired'
        ]);
    }

    public function complete()
    {
        $this->update([
            'status' => 'complete'
        ]);
    }

}
