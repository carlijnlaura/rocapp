<?php

namespace App\Models;

class Student extends User
{

    protected $table = 'users';

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('type', '=', 'student');
    }

}
