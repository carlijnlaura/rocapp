<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Training extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'niveau',
        'sector',
        'leerweg',
        'duration',
        'description',
    ];

    static $roles = [
        0 => 'guest',
        1 => 'student',
        2 => 'bpv',
        3 => 'teacher'
    ];


    static $trajects = [
        'BOL',
        'BBL',
    ];


    static $rules = [
        'title' => 'required|max:255',
        'niveau' => 'required|int',
        'sector' => 'required|max:255',
        'leerweg' => 'required',
        'duration' => 'required|int',
        'description' => 'required'
        ];



    public function teachers()
    {
        return $this->belongsToMany(User::class)->wherePivot('user_role', '=', 3);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }


}
