<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TrainingUser extends Pivot
{
    protected $fillable = [
        'user_id',
        'training_id',
        'user_role'
    ];

    static $roles = [
        0 => 'guest',
        1 => 'student',
        2 => 'bpv',
        3 => 'teacher'
    ];

    use HasFactory;

    /*public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function training()
    {
        return $this->belongsToMany(Training::class);
    }*/
}
