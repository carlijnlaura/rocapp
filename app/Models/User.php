<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'company_name',
        'phone_number',
        'website',
        'kvk',
        'type',
        'street',
        'zipcode',
        'place',
    ];

    static $defaultRole = 'student';

    static $roles = [
        0 => 'guest',
        1 => 'student',
        2 => 'bpv',
        3 => 'teacher'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullName()
    {
        return $this->type === 'bpv' ? $this->company_name : $this->first_name . ' ' . $this->last_name;
    }

    public function getAvatar()
    {
        if (File::exists(storage_path('app/public/avatars/' . $this->id . '.png'))) {
            return '/storage/avatars/' . $this->id . '.png';
        }
        return 'https://eu.ui-avatars.com/api/?rounded=true&background=ed7327&name=' . $this->getFullName() . '&color=ffffff&size=128';
    }

    public function isRole($role)
    {
        return $this->type === $role;
    }

    public function training()
    {
        return $this->belongsToMany(Training::class, 'training_user', 'user_id','training_id', 'id');
    }

    public function hours()
    {
        return $this->hasMany(Hour::class);
    }

    public function Internships()
    {
        return $this->hasMany(Internship::class, 'user_id', 'id');
    }


    public function trainings()
    {
        return $this->belongsToMany(Training::class);
    }

}
