<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default deployment strategy
    |--------------------------------------------------------------------------
    |
    | This option defines which deployment strategy to use by default on all
    | of your hosts. Laravel Deployer provides some strategies out-of-box
    | for you to choose from explained in detail in the documentation.
    |
    | Supported: 'basic', 'firstdeploy', 'local', 'pull'.
    |
    */

    'default' => 'basic',

    /*
    |--------------------------------------------------------------------------
    | Custom deployment strategies
    |--------------------------------------------------------------------------
    |
    | Here, you can easily set up new custom strategies as a list of tasks.
    | Any key of this array are supported in the `default` option above.
    | Any key matching Laravel Deployer's strategies overrides them.
    |
    */

    'strategies' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Hooks
    |--------------------------------------------------------------------------
    |
    | Hooks let you customize your deployments conveniently by pushing tasks
    | into strategic places of your deployment flow. Each of the official
    | strategies invoke hooks in different ways to implement their logic.
    |
    */

    'hooks' => [
        // Right before we start deploying.
        'start' => [
            //
        ],

        // Code and composer vendors are ready but nothing is built.
        'build' => [
            'ls:clear',
            'npm:install',
            'npm:production',
        ],

        // Deployment is done but not live yet (before symlink)
        'ready' => [
            'artisan:view:clear',
            'artisan:storage:link',
            'artisan:cache:clear',
            'artisan:config:cache',
            'artisan:migrate:fresh',
        ],

        // Deployment is done and live
        'done' => [
            //
        ],

        // Deployment succeeded.
        'success' => [
            //
        ],

        // Deployment failed.
        'fail' => [
            //
        ],

        // After a deployment has been rolled back.
        'rollback' => [
            //
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Deployment options
    |--------------------------------------------------------------------------
    |
    | Options follow a simple key/value structure and are used within tasks
    | to make them more configurable and reusable. You can use options to
    | configure existing tasks or to use within your own custom tasks.
    |
    */

    'options' => [
        'application' => env('APP_NAME', 'Laravel'),
        'repository' => 'https://2x2vkcfv4ealcy4taq4x4b4ucjg7fquxnt66tlydxwr6go7frspa@ictwf.visualstudio.com/2020_ADSD_Project_Frameworks_TeamB1/_git/2020_ADSD_Project_Frameworks_TeamB1',
        'writable_mode' => 'chmod',
        'writable_chmod_mode' => 7777,
        'env' => [
            "APP_NAME" => "RocApp",
            "APP_ENV" => "local",
            "APP_KEY" => "base64:4G3Lk2m5lpav4r29/k+dJh0N767Qic+HbRyi+0iuvc5=",
            "APP_DEBUG" => "true",
            "APP_URL" => "http://teamb1.zorg.adsd2020.clow.nl/",
            "LOG_CHANNEL" => "stack",
            "LOG_LEVEL" => "debug",
            "DB_CONNECTION" => "mysql",
            "DB_HOST" => "127.0.0.1",
            "DB_PORT" => "3306",
            "DB_DATABASE" => "teamb1_zorg",
            "DB_USERNAME" => "teamb1",
            "DB_PASSWORD" => "teamb1",
            "BROADCAST_DRIVER" => "log",
            "CACHE_DRIVER" => "file",
            "QUEUE_CONNECTION" => "sync",
            "SESSION_DRIVER" => "file",
            "SESSION_LIFETIME" => "120",
            "REDIS_HOST" => "127.0.0.1",
            "REDIS_PASSWORD" => "null",
            "REDIS_PORT" => "6379",
            "MAIL_MAILER" => "smtp",
            "MAIL_HOST" => " ",
            "MAIL_PORT" => "2525",
            "MAIL_USERNAME" => " ",
            "MAIL_PASSWORD" => " ",
            "MAIL_ENCRYPTION" => "tls",
            "MAIL_FROM_ADDRESS" => "noreply@teamb1.zorg.adsd2020.clow.nl",
            "MAIL_FROM_NAME" => "noreply",
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Hosts
    |--------------------------------------------------------------------------
    |
    | Here, you can define any domain or subdomain you want to deploy to.
    | You can provide them with roles and stages to filter them during
    | deployment. Read more about how to configure them in the docs.
    |
    */

    'hosts' => [
        'adsd2020.clow.nl' => [
            'deploy_path' => '/home/teamb1/public_html/zorg',
            'user' => 'teamb1',
            'branch' => 'master',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Localhost
    |--------------------------------------------------------------------------
    |
    | This localhost option give you the ability to deploy directly on your
    | local machine, without needing any SSH connection. You can use the
    | same configurations used by hosts to configure your localhost.
    |
    */

    'localhost' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Include additional Deployer recipes
    |--------------------------------------------------------------------------
    |
    | Here, you can add any third party recipes to provide additional tasks,
    | options and strategies. Therefore, it also allows you to create and
    | include your own recipes to define more complex deployment flows.
    |
    */

    'include' => [
        'recipe/my_recipe.php',
    ],

    /*
    |--------------------------------------------------------------------------
    | Use a custom Deployer file
    |--------------------------------------------------------------------------
    |
    | If you know what you are doing and want to take complete control over
    | Deployer's file, you can provide its path here. Note that, without
    | this configuration file, the root's deployer file will be used.
    |
    */

    'custom_deployer_file' => false,

];
