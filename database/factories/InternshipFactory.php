<?php

namespace Database\Factories;

use App\Models\BPV;
use App\Models\Internship;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class InternshipFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Internship::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 5000),
            'company_id' => BPV::inRandomOrder()->first()->id,
            'start_date' => $this->faker->date,
            'end_date' => $this->faker->date,
            'hours' => $this->faker->numberBetween(0, 1000),
            'function' => $this->faker->jobTitle,
            'status' => $this->faker->randomElement(Internship::$availableStatus)
        ];
    }
}
