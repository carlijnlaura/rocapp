<?php

namespace Database\Factories;

use App\Models\Training;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Training::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->jobTitle,
            'niveau' => $this->faker->numberBetween(1, 5),
            'sector' => $this->faker->randomElement([
                'Agrarisch & Milieu',
                'Algemeen',
                'Architectuur & Design',
                'Automotive',
                'Banken',
                'Betting & Gaming',
                'Beveiliging',
                'Bouw',
                'Chemie, Olie & Energie',
                'Defensie',
                'Dieren & Verzorging',
                'Engineering',
                'Erotiek',
                'Facilitaire Dienstverlening',
                'Fashion & Styling',
                'Finance',
                'FMCG',
                'Gemeente',
                'Gezondheidszorg & Farmacie',
                'Handel, Groothandel & Detailhandel',
                'Horeca',
                'ICT',
                'Industrie & Productie',
                'Industrie & Productie',
                'Juridisch',
                'Kunst & Cultuur & Entertainment',
                'Life Sciences',
                'Luchtvaart & Zeevaart',
                'Marketing & Communicatie',
                'Media & Journalistiek',
                'Non-profit',
                'Onderwijs',
                'Onderzoek',
                'Overheid & Semi-overheid',
                'Politie & Beveiliging',
                'Reizen & Recreatie',
                'Techniek',
                'Transport & Logistiek',
                'Uitzendbureaus & Werving & Selectie',
                'Vastgoed & Makelaardij',
                'Voeding & Beweging',
                'Zakelijke Dienstverlening',
            ]),
            'leerweg' => $this->faker->randomElement(Training::$trajects),
            'duration' => $this->faker->numberBetween(1, 5),
            'description' => $this->faker->realText(),
        ];
    }
}
