<?php

namespace Database\Factories;

use App\Models\Training;
use App\Models\TrainingUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TrainingUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'training_id' => $this->faker->numberBetween(1, 1000),
            'user_role' => $this->faker->randomElement(array_keys(User::$roles))
        ];
    }
}
