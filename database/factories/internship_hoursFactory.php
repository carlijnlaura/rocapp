<?php

namespace Database\Factories;

use App\Models\BPV;
use App\Models\Internship;
use App\Models\internship_hours;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class internship_hoursFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = internship_hours::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'userid' => $this->faker->numberBetween(0, 5000),
            'intern_id' => Internship::inRandomOrder()->first()->id,
            'hours' => $this->faker->numberBetween(1, 100),
            'date' => $this->faker->dateTime,
            'begintime' => $this->faker->time(),
            'endtime' => $this->faker->time()
        ];
    }
}
