<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hours', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('userid');
            $table->integer('intern_id');
            $table->string('hours');
            $table->timestamp('date');
            $table->time('begintime');
            $table->time('endtime');
            $table->enum('status', \App\Models\Hour::$answers)->default(\App\Models\Hour::$defaultAnswer);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hours');
    }
}
