<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateinternshipHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internship_hours', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('userid');
            $table->integer('intern_id');
            $table->string('hours');
            $table->timestamp('date');
            $table->time('begintime');
            $table->time('endtime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internship_hours');
    }
}
