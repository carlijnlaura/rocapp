<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5000)->create();
        \App\Models\Training::factory(100)->create();
        \App\Models\Internship::factory(1000)->create();
        \App\Models\internship_hours::factory(1000)->create();
//        \App\Models\TrainingUser::factory(1000)->create();
    }
}
