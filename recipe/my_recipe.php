<?php

namespace Deployer;

task('ls:clear', function () {
    exec('rm -rf /home/teamb1/public_html/zorg/shared/storage/logs/*');
    exec('rm -rf /home/teamb1/public_html/zorg/shared/storage/framework/sessions/*');
});
