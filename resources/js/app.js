require('./bootstrap');
window.jquery = window.jQuery = window.$ = require('jquery/dist/jquery')
require('bootstrap/dist/js/bootstrap.bundle.min')
require('./jquery.dataTables.min')
require('./iDashboard')
window.ApexCharts = require('./apexcharts.min')

$(document).ready(function () {
    if (location.hash) {
        $("a[href='" + location.hash + "']").tab("show");
    }
    $(document.body).on("click", "a[data-toggle='tab']", function (event) {
        location.hash = this.getAttribute("href");
    });
});
$(window).on("popstate", function () {
    var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
    $("a[href='" + anchor + "']").tab("show");
});
//
// var options = {
//     series: [75],
//     chart: {
//         height: 350,
//         type: 'radialBar',
//         toolbar: {
//             show: false
//         }
//     },
//     plotOptions: {
//         radialBar: {
//             startAngle: -135,
//             endAngle: 225,
//             hollow: {
//                 margin: 0,
//                 size: '70%',
//                 background: '#fff',
//                 image: undefined,
//                 imageOffsetX: 0,
//                 imageOffsetY: 0,
//                 position: 'front',
//                 dropShadow: {
//                     enabled: true,
//                     top: 3,
//                     left: 0,
//                     blur: 4,
//                     opacity: 0.1
//                 }
//             },
//             track: {
//                 background: '#fff',
//                 strokeWidth: '67%',
//                 margin: 0,
//             },
//
//             dataLabels: {
//                 show: true,
//                 name: {
//                     offsetY: -10,
//                     show: true,
//                     color: '#888',
//                     fontSize: '17px'
//                 },
//                 value: {
//                     formatter: function (val) {
//                         return parseInt(val);
//                     },
//                     color: '#111',
//                     fontSize: '36px',
//                     show: true,
//                 }
//             }
//         }
//     },
//     fill: {
//         colors: '#ed7327',
//         opacity: 1,
//     },
//     stroke: {
//         lineCap: 'round'
//     },
//     labels: ['Competenties'],
// };
//
// var chart = new ApexCharts(document.querySelector("#chart"), options);
// chart.render();
//
// options = null;
//
// var options = {
//     series: [{
//         name: "STOCK ABC",
//         data: [1,2,3]
//     }],
//     chart: {
//         type: 'area',
//         height: 350,
//         width: '100%',
//         zoom: {
//             enabled: false
//         },
//         toolbar: {
//             show: false
//         }
//     },
//     dataLabels: {
//         enabled: false
//     },
//     stroke: {
//         curve: 'straight'
//     },
//
//     labels: ['2020-10-10 10:10:10','2020-10-09 10:10:10','2020-10-09 10:10:10'],
//     xaxis: {
//         type: 'datetime',
//     },
//     yaxis: {
//         opposite: true
//     },
//     legend: {
//         horizontalAlign: 'left'
//     }
// };
//
// var chart = new ApexCharts(document.querySelector("#chart2"), options);
// chart.render();


$('input[name="avatar"]').on('change', function () {
    let fd = new FormData();
    let files = $('#avatar')[0].files[0];
    fd.append('file', files);
    $.ajax({
        url: '/update-avatar',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function (response) {
            window.location.reload();
        },
    })
});

$(document).on('click', '#refresh-maps', function () {
    let company_name = $('input[name="company_name"]').val() === '' ? 'MBO College Almere' : $('input[name="company_name"]').val();
    let street = $('input[name="street"]').val() === '' ? 'Straat van Florida 1' : $('input[name="street"]').val();
    let zipcode = $('input[name="zipcode"]').val() === '' ? '1334 PA' : $('input[name="zipcode"]').val();
    let place = $('input[name="place"]').val() === '' ? 'Almere' : $('input[name="place"]').val();
    let iframe = $('iframe');
    iframe.attr('src', 'https://maps.google.com/maps/embed/v1/place?key=AIzaSyD4f8KjyHs9GWJBX6VH_lQIj2hZzImkYRU&q=' + company_name + ',' + street + ',' + zipcode + ',' + place);
});

$('a').each(function () {
    if ($(this).attr('href') === window.location.pathname || $(this).attr('href') === window.location.href) {
        $(this).parent().addClass('active');
    }
})
