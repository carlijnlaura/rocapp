<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Je wachtwoord is geupdate!',
    'sent' => 'We hebben een email gestuurd met een reset link!',
    'throttled' => 'Je hebt laatst nog deze actie gedaan. Probeer later opnieuw.',
    'token' => 'Deze reset token is niet correct.',
    'user' => "We kunnen geen gebruiker vinden met dit email address.",

];
