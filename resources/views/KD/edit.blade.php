
@extends('layouts.app')

@section('content')
<div class="card" style="width: 36rem;">
<form action="{{route('KD.update', $KD->id)}}" method="post" enctype="multipart/form-data" id="updateKD">
    @csrf
    @method('PUT')
    <div class="card-body">
               

                <div class="form-group">
                <select name="training_id" id="training_id">
                <option value="Null">-- Selecteer training --</option>
                 @foreach($trainingen as $training )
                <option value="{{ $training->id }}">{{ $training->title }}</option>
                 @endforeach
                 </select>
                </div>
                

                
                <div class="form-group">
                <select name="user_id" id="user_id">
                <option value="Null">-- Selecteer Student --</option>
                 @foreach($users as $user )
                <option value="{{ $user->id }}">{{ $user->first_name . " " . $user->last_name }}</option>
                 @endforeach
                 </select>
                </div>
                

                <div class="col-sm-9">
                <input class="form-control" type="file" name="dossier" id="dossier">
                </div>
         
    </div>

</form>
<div class="modal-footer">
        <button type="submit" class="btn btn-primary" form="updateKD"> Opslaan </button>
        <form action="{{route('KD.destroy', [$KD->id])}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">verwijder<i class="fas fa-trash"></i></button>
    </form>
    </div>

@endsection