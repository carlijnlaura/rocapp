

@extends('layouts.app')

@section('content')
<a href="{{route('KD.create')}}">Create dossier</a>
@if(count($dossiers))            
<div>
@foreach($dossiers as $kd)
<div class="col-3">
<div class="card cursor-pointer">
<p> KD van= {{$kd->first_name}} {{$kd->last_name}}</p>
<button onclick="window.location.href = '/KD/{{$kd->id}}'">view</button><button onclick="window.location.href = '{{route('KD.edit', [$kd->id])}}'">edit</button>
</div>
</div>
@endforeach
</div>
@else

        <p>geen dossiers gevonden!</p>  
        
               
        @endif
        
@endsection
