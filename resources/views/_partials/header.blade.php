<header class="yoo-header yoo-style1 yoo-sticky-menu">
    <div class="yoo-main-header">
        <div class="yoo-main-header-in">
            <div class="yoo-main-header-left">
                <a href="/" class="yoo-logo-link yoo-light-logo"><img
                        src="https://www.rocvanflevoland.nl/assets/img/og-logo-rocvf.png"
                        alt="logo-light"/></a>
            </div>
            <div class="yoo-main-header-right">
                <div class="yoo-nav-wrap yoo-fade-up">
                </div>
                <ul class="yoo-ex-nav yoo-style1 yoo-flex yoo-mp0">
                    <li>
                        <div class="yoo-toggle-body yoo-profile-nav yoo-style1 border-0 pl-0">
                            <div class="yoo-toggle-btn yoo-profile-nav-btn">
                                <div class="yoo-profile-nav-text">
                                    <span>Welkom,</span>
                                    <h4>{{auth()->user()->getFullName()}}</h4>
                                </div>
                                <div class="yoo-profile-nav-img">
                                    <img
                                        src="{{auth()->user()->getAvatar()}}"
                                        alt="profile">
                                </div>
                            </div>
                            <ul class="yoo-dropdown yoo-style1">
                                <li>
                                    <a href="/profile">
                                        <ion-icon name="person-circle"></ion-icon>
                                        Mijn Profiel</a>
                                </li>
                                <li class="yoo-dropdown-cta"><a href="#" class="text-danger" onclick="$('#logout-form').submit();">Log uit</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
