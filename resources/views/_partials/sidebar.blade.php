<div class="yoo-sidebarheader-toggle">
    <div class="yoo-button-bar1"></div>
    <div class="yoo-button-bar2"></div>
    <div class="yoo-button-bar3"></div>
</div>
<div class="yoo-sidebarheader">
    <div class="yoo-sidebarheader-in">
        <div class="yoo-sidebar-nav">
            <ul class="yoo-sidebar-nav-list yoo-mp0">
                <div class="yoo-sidebar-nav-title">
                    <?php if(auth()->user()->isRole('teacher')) : ?>
                    <span class="yoo-sidebar-nav-title-text">Studenten</span>
                        <?php endif; ?>
                        <span class="yoo-sidebar-nav-title-dotline">
                        <ion-icon name="ellipsis-horizontal" role="img" class="md hydrated"></ion-icon>
                    </span>
                </div>
                <?php if(auth()->user()->isRole('teacher')) : ?>
                <li>
                    <a href="{{route('students.index')}}">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="school-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Studenten</span>
                        </span>
                    </a>
                </li>
                <?php endif; ?>

                <div class="yoo-sidebar-nav-title">
                    <span class="yoo-sidebar-nav-title-text">Bedrijven</span>
                    <span class="yoo-sidebar-nav-title-dotline">
                        <ion-icon name="ellipsis-horizontal" role="img" class="md hydrated"></ion-icon>
                    </span>
                </div>
                <?php if(auth()->user()->isRole('teacher')) : ?>
                <li>
                    <a href="/school-company">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="laptop-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Bedrijven</span>
                        </span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(auth()->user()->isRole('bpv')) : ?>
                <li>
                    <a href="/companies">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="laptop-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Bedrijven</span>
                        </span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(auth()->user()->isRole('student')) : ?>
                <li>
                    <a href="/companies">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="laptop-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Bedrijven</span>
                        </span>
                    </a>
                </li>
                <?php endif; ?>

                <div class="yoo-sidebar-nav-title">
                    <span class="yoo-sidebar-nav-title-text">Opleiding</span>
                    <span class="yoo-sidebar-nav-title-dotline">
                        <ion-icon name="ellipsis-horizontal" role="img" class="md hydrated"></ion-icon>
                    </span>
                </div>
                <li>
                    <a href="{{route('trainings.index')}}">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="newspaper-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Opleidingen</span>
                        </span>
                    </a>
                </li>
                <?php if(auth()->user()->isRole('teacher')) : ?>
                <li>
                    <a href="{{route('KD.index')}}">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="layers"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">kwalificatieDossiers</span>
                        </span>
                    </a>
                </li>
                <?php endif; ?>
                @if(auth()->user()->isRole('student'))
                <div class="yoo-sidebar-nav-title">
                    <span class="yoo-sidebar-nav-title-text">Mijn voortgang</span>
                    <span class="yoo-sidebar-nav-title-dotline">
                        <ion-icon name="ellipsis-horizontal" role="img" class="md hydrated"></ion-icon>
                    </span>
                </div>
                <li>
                    <a href="{{ route('hours.index') }}">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="time-outline"></ion-icon>
                            </span>

                            <span class="yoo-sidebar-link-text">Urenlijst</span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="KD.showStudentKD">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="folder-outline"></ion-icon>
                            </span>
                            <span class="yoo-sidebar-link-text">Kwalificatiedossier</span>
                        </span>
                    </a>
                </li>
                    <li>
                        <a href="/company/requests">
                            <span class="yoo-sidebar-link-title">
                                <span class="yoo-sidebar-link-icon yoo-style1">
                                    <ion-icon name="git-pull-request-outline"></ion-icon>
                                </span>
                                <span class="yoo-sidebar-link-text">Aanvragen</span>
                            </span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->isRole('bpv'))
                    <div class="yoo-sidebar-nav-title">
                        <span class="yoo-sidebar-nav-title-text">Mijn bedrijf</span>
                        <span class="yoo-sidebar-nav-title-dotline">
                            <ion-icon name="ellipsis-horizontal" role="img" class="md hydrated"></ion-icon>
                        </span>
                    </div>
                    <li>
                        <a href="/my-company/requests">
                            <span class="yoo-sidebar-link-title">
                                <span class="yoo-sidebar-link-icon yoo-style1">
                                    <ion-icon name="git-pull-request-outline"></ion-icon>
                                </span>
                                <span class="yoo-sidebar-link-text">Aanvragen</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/my-company/stagaires">
                            <span class="yoo-sidebar-link-title">
                                <span class="yoo-sidebar-link-icon yoo-style1">
                                    <ion-icon name="briefcase-outline"></ion-icon>
                                </span>
                                <span class="yoo-sidebar-link-text">Stagaires</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('hours.index') }}">
                        <span class="yoo-sidebar-link-title">
                            <span class="yoo-sidebar-link-icon yoo-style1">
                                <ion-icon name="time-outline"></ion-icon>
                            </span>

                            <span class="yoo-sidebar-link-text">Urenlijsten Studenten</span>
                        </span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
