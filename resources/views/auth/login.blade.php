@extends('layouts.auth')

@section('content')
<div class="yoo-login-wrap yoo-style3 yoo-bg">
    <form action="{{route('login')}}" method="post" class="yoo-form yoo-style2 yoo-border">
        @csrf
        <div class="yoo-form-logo">
            <img style="height: 70px;" src="https://www.rocvanflevoland.nl/assets/img/og-logo-rocvf.png" alt="">
        </div>
        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
        <h2 class="yoo-form-title">Log in om verder te gaan</h2>
        <div class="yoo-form-subtitle">Nog geen account? <a href="{{route('register')}}" class="yoo-form-btn yoo-style2">Registreer
                hier</a></div>
        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Wachtwoord</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="yoo-forget-pass-wrap">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="gridCheck">
                            <label class="custom-control-label" for="gridCheck">
                                <span class="custom-control-shadow"></span>Onthoud mij
                            </label>
                        </div>
                        <a href="/password/reset" class="yoo-form-btn yoo-style2">Wachtwoord vergeten?</a>
                    </div>
                </div>
                <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Log in</span></button>
            </div>
        </div>
    </form>
</div>
@endsection
