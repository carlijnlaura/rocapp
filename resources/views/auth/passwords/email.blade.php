@extends('layouts.auth')

@section('content')
    <div class="yoo-login-wrap yoo-style3 yoo-bg">
        <form action="{{route('password.email')}}" method="post" class="yoo-form yoo-style2 yoo-border">
            @csrf
            <h2 class="yoo-form-title">Reset wachtwoord</h2>
            <div class="yoo-height-b25 yoo-height-lg-b25"></div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="email">E-mailadres</label>
                        <input type="text" name="email" class="form-control" id="email">
                    </div>
                    @error('email')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }} <br>
                            Klik <a href="/login" class="text-success">hier</a> om terug te gaan naar de login pagina
                        </div>
                    @endif
                    <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Stuur email</span></button>
                </div>
            </div>
        </form>
    </div>
@endsection
