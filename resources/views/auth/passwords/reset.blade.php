@extends('layouts.auth')

@section('content')

    <div class="yoo-login-wrap yoo-style3 yoo-bg">
        <form action="{{route('password.update')}}" method="post" class="yoo-form yoo-style2 yoo-border">
            @csrf

            <input type="hidden" name="email" value="{{ $email }}" required autocomplete="email">
            <input type="hidden" name="token" value="{{ $token }}">

            <h2 class="yoo-form-title">Reset wachtwoord</h2>
            <div class="yoo-height-b25 yoo-height-lg-b25"></div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="password">Wachtwoord</label>
                        <input type="password" name="password" class="form-control" id="password" autocomplete="new-password">
                    </div>
                    @error('password')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                    <div class="form-group level-up form-group-md">
                        <label for="password_confirmation">Bevestig wachtwoord</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" autocomplete="new-password">
                    </div>
                    @error('password_confirmation')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                    <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Update wachtwoord</span></button>
                </div>
            </div>
        </form>
    </div>

@endsection
