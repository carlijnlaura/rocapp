@extends('layouts.auth')

@section('content')

    <div class="yoo-login-wrap yoo-style1 yoo-bg yoo-dynamicbg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 yoo-form yoo-style2" id="regcontainer">
                    <div class="yoo-form-logo">
                        <img style="height: 70px;" src="https://www.rocvanflevoland.nl/assets/img/og-logo-rocvf.png"
                             alt="">
                    </div>
                    <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                    <h2 class="yoo-form-title">Maak een account aan</h2>
                    <div class="yoo-form-subtitle">Heb je al een account? <a href="{{route('login')}}"
                                                                             class="yoo-form-btn yoo-style2">Log
                            in</a></div>
                    <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs nav-fill">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#nav-student" data-toggle="tab" onclick="$('#regcontainer').css('max-width', '500px');" role="tab">
                                        <ion-icon name="school-outline" class="mr-2"></ion-icon>
                                        Student</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#nav-bpv" data-toggle="tab" role="tab" onclick="$('#regcontainer').css('max-width', 'unset');">
                                        <ion-icon name="bag-outline" class="mr-2"></ion-icon>
                                        BPV</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-student" role="tabpanel">
                                    @include('auth.register.student')
                                </div>
                                <div class="tab-pane fade" id="nav-bpv" role="tabpanel">
                                    @include('auth.register.bpv')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
