<form action="{{route('register')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-6">
            <input type="hidden" name="type" value="bpv" id="register_type">
            <div class="row" style="margin-top: 20px;">
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="company_name">Bedrijfsnaam</label>
                        <input type="text" class="form-control" name="company_name" id="company_name">
                    </div>
                    @error('company_name')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" id="email">
                    </div>
                    @error('email')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-8">
                    <div class="form-group level-up form-group-md">
                        <label for="street">Straat</label>
                        <input type="text" class="form-control" name="street" id="street">
                    </div>
                    @error('street')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-4">
                    <div class="form-group level-up form-group-md">
                        <label for="zipcode">Postcode</label>
                        <input type="text" class="form-control" name="zipcode" id="numbezipcode">
                    </div>
                    @error('zipcode')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="place">Plaats</label>
                        <input type="text" class="form-control" name="place" id="place">
                    </div>
                    @error('place')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="email">Telefoon nummer</label>
                        <input type="number" class="form-control" name="phone_number" id="email">
                    </div>
                    @error('phone_number')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="email">Website</label>
                        <input type="text" class="form-control" name="website" id="email">
                    </div>
                    @error('website')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="email">KVK-Nummer</label>
                        <input type="number" class="form-control" name="kvk" id="email">
                    </div>
                    @error('kvk')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="password">Wachtwoord</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>
                    @error('password')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-lg-12">
                    <div class="form-group level-up form-group-md">
                        <label for="password">Bevestig wachtwoord</label>
                        <input type="password" class="form-control" name="password_confirmation"
                               id="password">
                    </div>
                    @error('password_confirmation')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-6">
            <iframe width="100%" style="margin-top: 20px" height="476px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="rounded"
                    src="https://maps.google.com/maps/embed/v1/place?key=AIzaSyD4f8KjyHs9GWJBX6VH_lQIj2hZzImkYRU&q=Straat+van+Florida+1+1334+PA+Almere"></iframe>
            <button style="margin-top: 20px;margin-bottom:20px;" type="button" id="refresh-maps" class="yoo-form-btn yoo-style1 yoo-color3"><span>Refresh maps</span></button>
        </div>
    </div>

    <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Registreer</span></button>
</form>
