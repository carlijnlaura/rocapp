<form action="{{route('register')}}" method="POST">
    @csrf
    <input type="hidden" name="type" value="student" id="register_type">
    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6">
            <div class="form-group level-up form-group-md">
                <label for="first-name">Voornaam</label>
                <input type="text" class="form-control" name="first_name" id="first-name">
            </div>
            @error('first_name')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-6">
            <div class="form-group level-up form-group-md">
                <label for="last-name">Achternaam</label>
                <input type="text" class="form-control" name="last_name" id="last-name">
            </div>
            @error('last_name')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-12">
            <div class="form-group level-up form-group-md">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email">
            </div>
            @error('email')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-12">
            <div class="form-group level-up form-group-md">
                <label for="password">Wachtwoord</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            @error('password')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-12">
            <div class="form-group level-up form-group-md">
                <label for="password">Bevestig wachtwoord</label>
                <input type="password" class="form-control" name="password_confirmation"
                       id="password">
            </div>
            @error('password_confirmation')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Registreer</span></button>
</form>
