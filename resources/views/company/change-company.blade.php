@extends('layouts.app')

@section('content')
    @foreach($users as $user)
    <form action="{{ url('/changed-company'.$user->id) }}" method="POST">
        @endforeach
        @csrf
        @method('get')
        <input type="hidden" name="type" value="bpv" id="register_type">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="company_name"></label>
                    <input type="text" class="form-control" name="company_name" id="company_name" {{$company_name = $user->company_name}} value="{{$company_name}}">
                </div>
                @error('company_name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email"></label>
                    <input type="text" class="form-control" name="email" id="email" {{$email = $user->email}} value="{{$email}}">
                </div>
                @error('email')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email"></label>
                    <input type="number" class="form-control" name="phone_number" id="email"{{$phone_number = $user->phone_number}} value="{{$phone_number}}">
                </div>
                @error('phone_number')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-8">
            <div class="form-group level-up form-group-md">
                <label for="street"></label>
                <input type="text" class="form-control" name="street" id="street"{{$street = $user->street}} value="{{$street}}">
            </div>
            @error('street')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-4">
            <div class="form-group level-up form-group-md">
                <label for="zipcode"></label>
                <input type="text" class="form-control" name="zipcode" id="numbezipcode"{{$zipcode = $user->zipcode}} value="{{$zipcode}}">
            </div>
            @error('zipcode')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-12">
            <div class="form-group level-up form-group-md">
                <label for="place"></label>
                <input type="text" class="form-control" name="place" id="place"{{$place = $user->place}} value="{{$place}}">
            </div>
            @error('place')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email"></label>
                    <input type="text" class="form-control" name="website" id="email"{{$website = $user->website}} value="{{$website}}">
                </div>
                @error('website')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email"></label>
                    <input type="number" class="form-control" name="kvk" id="email"{{$kvk = $user->kvk}} value="{{$kvk}}">
                </div>
                @error('kvk')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Wachtwoord</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                @error('password')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Bevestig wachtwoord</label>
                    <input type="password" class="form-control" name="password_confirmation"
                           id="password">
                </div>
                @error('password_confirmation')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>verander bedrijfsgegevens</span></button>
    </form>

@endsection
