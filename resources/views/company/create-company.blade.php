@extends('layouts.app')

@section('content')
    <form action="{{route('create.company')}}" method="POST">
        @csrf
        @method('get')
        <input type="hidden" name="type" value="bpv" id="register_type">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="company_name">Bedrijfsnaam</label>
                    <input type="text" class="form-control" name="company_name" id="company_name">
                </div>
                @error('company_name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" id="email">
                </div>
                @error('email')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Telefoon nummer</label>
                    <input type="number" class="form-control" name="phone_number" id="email">
                </div>
                @error('phone_number')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-8">
            <div class="form-group level-up form-group-md">
                <label for="street">Straat</label>
                <input type="text" class="form-control" name="street" id="street">
            </div>
            @error('street')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-4">
            <div class="form-group level-up form-group-md">
                <label for="zipcode">Postcode</label>
                <input type="text" class="form-control" name="zipcode" id="numbezipcode">
            </div>
            @error('zipcode')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="col-lg-12">
            <div class="form-group level-up form-group-md">
                <label for="place">Plaats</label>
                <input type="text" class="form-control" name="place" id="place">
            </div>
            @error('place')
            <div class="alert alert-danger" role="alert">
                {{ $message }}
            </div>
            @enderror
        </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Website</label>
                    <input type="text" class="form-control" name="website" id="email">
                </div>
                @error('website')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">KVK-Nummer</label>
                    <input type="number" class="form-control" name="kvk" id="email">
                </div>
                @error('kvk')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Wachtwoord</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                @error('password')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Bevestig wachtwoord</label>
                    <input type="password" class="form-control" name="password_confirmation"
                           id="password">
                </div>
                @error('password_confirmation')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Maak nieuw bedrijf aan</span></button>
    </form>
@endsection
