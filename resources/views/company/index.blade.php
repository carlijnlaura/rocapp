@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Bedrijven</h2>
            <form action="/companies" method="get" class="mb-0">
                <div class="form-group level-up mb-0">
                    <label for="exampleInputEmail18">Zoek bedrijven</label>
                    <input type="search" class="form-control" id="exampleInputEmail18" name="q">
                </div>
            </form>
            @if(\request()->has('q'))
                <a href="/companies" class="btn btn-outline-primary">Reset resultaaten</a>
            @endif
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
            @if(count($companies))
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Telefoon-nummer</th>
                                <th>Website</th>
                                <th>KVK nummer</th>
                                <th>Emailadres</th>
        <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($companies as $company)
                                <tr>
                                    <td>{{ $company->company_name }}</td>
                                    <td>{{ $company->phone_number }}</td>
                                    <td>{{ $company->website }}</td>
                                    <td>{{ $company->kvk }}</td>
                                    <td>{{ $company->email }}</td>
                                    <td><a href="/companies/{{$company->id}}" class="btn btn-sm btn-primary"><ion-icon name="eye-outline"></ion-icon></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                    {!! $companies->links() !!}
                </div>
            @else
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Geen bedrijven gevonden
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
