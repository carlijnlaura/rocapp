@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Bedrijven</h2>
            <a class="btn btn-primary" href="{{ url('create-company') }}">Voeg een bedrijf toe</a>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
            @if(count($users))
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Telefoon-nummer</th>
                                <th>Website</th>
                                <th>KVK nummer</th>
                                <th>Emailadres</th>
                                <th width="280px" class="text-right">Acties</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->company_name }}</td>
                                    <td>{{ $user->phone_number }}</td>
                                    <td>{{ $user->website }}</td>
                                    <td>{{ $user->kvk }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-right">
                                        <button class="btn-sm btn-info btn-disabled">
                                            <ion-icon class="text-white" name="eye-outline"></ion-icon>
                                        </button>
                                        <a href="change-company">
                                        <button class="btn-sm btn-primary">
                                            <ion-icon name="create-outline"></ion-icon>
                                        </button>
                                        </a>
                                        <a href="{{ url('delete-company',$user->id) }}">
                                        <button type="submit" class="btn-sm btn-danger">
                                                <ion-icon name="trash-outline"></ion-icon>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                </div>
            @else
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Geen bedrijven gevonden
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
