@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Bedrijf informatie</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1 ">
            <div class="yoo-padd-lr-30">
                <div class="yoo-height-b30 yoo-height-lg-b30"></div>
                <div class="yoo-user yoo-style4">
                    <div class="yoo-user-img">
                        <img src="{{$company->getAvatar()}}" alt="">
                    </div>
                    <div class="yoo-user-info">
                        <div>
                            <h3 class="yoo-user-name">{{$company->company_name}}</h3>
                            <span class="yoo-lavel">Bedrijf</span>
                            <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        </div>
                        <p class="ml-auto">
                            <a href="/company/{{$company->id}}/request" class="btn btn-primary">Vraag stageplek aan</a>
                        </p>
                    </div>
                </div>
                <div class="yoo-height-b30 yoo-height-lg-b30"></div>
                <hr>
                <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                <ul class="yoo-contact-info-list yoo-mp0">
                    <li>
                        <div class="yoo-contact-info-label">Email</div>
                        <div class="yoo-contact-info-details">{{$company->email}}</div>
                    </li>
                    <li>
                        <div class="yoo-contact-info-label">KVK</div>
                        <div
                            class="yoo-contact-info-details">{{$company->kvk}}</div>
                    </li>
                </ul>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
            </div>
        </div>
        <div class="yoo-card yoo-style1 mt-4">
            <div class="yoo-padd-lr-30">
                <iframe width="100%" style="margin-top: 20px" height="476px" frameborder="0"
                        scrolling="no" marginheight="0" marginwidth="0" class="rounded"
                        src="https://maps.google.com/maps/embed/v1/place?key=AIzaSyD4f8KjyHs9GWJBX6VH_lQIj2hZzImkYRU&q={{$company->street}}+{{$company->zipcode}}+{{$company->place}}"></iframe>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
            </div>
        </div>
    </div>

@endsection
