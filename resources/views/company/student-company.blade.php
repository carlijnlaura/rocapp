@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Bedrijven</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        @if(count($users))
            <div class="yoo-card yoo-style1">
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        @foreach ($users as $user)
                            <a href="/companies/{{$user->id}}">
                                {{ $user->company_name }}
                            </a>
                        @endforeach
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
            </div>
        @else
            <div class="yoo-card yoo-style1">
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Geen bedrijven gevonden
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
