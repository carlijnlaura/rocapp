@extends('layouts.app')

@section('content')

    <style>
        .yoo-sidebarheader, .yoo-sidebarheader-toggle {
            display: none !important;
        }

        .yoo-content.yoo-style1 {
            padding-left: 0 !important;
        }

        .yoo-main-header-left {
            border-right: 0 !important;
        }

        .yoo-flex.p-5.d-flex.flex-column a:hover * {
            color: #ed7327 !important;
        }
    </style>

    @if(auth()->user()->isRole('student'))
        <div class="row">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/companies" class="text-center">
                                    <h1>
                                        <ion-icon name="laptop-outline"></ion-icon>
                                    </h1>
                                    <p>Bedrijven</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{route('trainings.index')}}" class="text-center">
                                    <h1>
                                        <ion-icon name="newspaper-outline"></ion-icon>
                                    </h1>
                                    <p>Opleidingen</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{ route('hours.index') }}" class="text-center">
                                    <h1>
                                        <ion-icon name="time-outline"></ion-icon>
                                    </h1>
                                    <p>Urenlijst</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="KD.showStudentKD" class="text-center">
                                    <h1>
                                        <ion-icon name="folder-outline"></ion-icon>
                                    </h1>
                                    <p>Kwalificatiedossier</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/company/requests" class="text-center">
                                    <h1>
                                        <ion-icon name="git-pull-request-outline"></ion-icon>
                                    </h1>
                                    <p>Aanvragen</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(auth()->user()->isRole('bpv'))

        <div class="row">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/companies" class="text-center">
                                    <h1>
                                        <ion-icon name="laptop-outline"></ion-icon>
                                    </h1>
                                    <p>Bedrijven</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{route('trainings.index')}}" class="text-center">
                                    <h1>
                                        <ion-icon name="newspaper-outline"></ion-icon>
                                    </h1>
                                    <p>Opleidingen</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{ route('hours.index') }}" class="text-center">
                                    <h1>
                                        <ion-icon name="time-outline"></ion-icon>
                                    </h1>
                                    <p>Urenlijst studenten</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/my-company/requests" class="text-center">
                                    <h1>
                                        <ion-icon name="git-pull-request-outline"></ion-icon>
                                    </h1>
                                    <p>Aanvragen</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/my-company/stagaires" class="text-center">
                                    <h1>
                                        <ion-icon name="briefcase-outline"></ion-icon>
                                    </h1>
                                    <p>Stagaires</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(auth()->user()->isRole('teacher'))
        <div class="row">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{route('students.index')}}" class="text-center">
                                    <h1>
                                        <ion-icon name="school-outline"></ion-icon>
                                    </h1>
                                    <p>Studenten</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="/school-company" class="text-center">
                                    <h1>
                                        <ion-icon name="laptop-outline"></ion-icon>
                                    </h1>
                                    <p>Bedrijven</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{route('trainings.index')}}" class="text-center">
                                    <h1>
                                        <ion-icon name="newspaper-outline"></ion-icon>
                                    </h1>
                                    <p>Opleidingen</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-padd-lr-20">
                            <div class="yoo-flex p-5 d-flex flex-column">
                                <a href="{{route('KD.index')}}" class="text-center">
                                    <h1>
                                        <ion-icon name="folder-outline"></ion-icon>
                                    </h1>
                                    <p>kwalificatieDossiers</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
