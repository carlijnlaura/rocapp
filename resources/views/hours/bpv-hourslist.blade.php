@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Urenlijst van alle studenten</h2>
            <a class="btn btn-primary" href="{{ route('hours.show') }}">voeg data toe</a>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>gebruikers</th>
                                <th>email</th>
                                <th>uren</th>
                                <th>datum</th>
                                <th>begintijd</th>
                                <th>eindtijd</th>
                                <th>stage</th>
                                <th>status</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($hour as $hours)
                            <tr>
                                <td>{{$hours->user->first_name. ' ' .$hours->user->last_name}}</td>
                                <td>{{$hours->user->email}}</td>
                                <td>{{$hours->hours}}</td>
                                <td>{{explode(' ', $hours->date)[0]}}</td>
                                <td>{{$hours->begintime}}</td>
                                <td>{{$hours->endtime}}</td>
                                <td>{{$hours->internship->Company->company_name}}</td>
                                <td>{{$hours->status}}</td {{$user = \Illuminate\Support\Facades\Auth::user()}}>
                                @if($user->id == $hours->internship->Company->id)
                                <td>
                                    <a href="good-hours{{$hours->id}}">
                                        <button class="btn-sm btn-success">
                                            klopt
                                        </button>
                                    </a>
                                </td>
                                <td>
                                    <a href="bad-hours{{$hours->id}}">
                                        <button type="submit" class="btn-sm btn-danger">
                                            fout
                                        </button>
                                    </a>
                                </td>
                                @else
                                    <td></td>
                                    <td></td>
                                @endif
                            </tr
                                @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                </div>
        </div>
    </div>

@endsection
