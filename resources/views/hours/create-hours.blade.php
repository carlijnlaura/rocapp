@extends('layouts.app')

@section('content')
    <form action="{{ route('hours.create') }}" method="POST">
        @csrf
        @method('get')
        <input type="hidden" name="type" value="bpv" id="register_type">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="company_name">Uren</label>
                    <input type="text" class="form-control" name="uren" id="uren">
                </div>
                @error('hours')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Datum</label>
                    <input type="date" class="form-control" name="datum" id="datum">
                </div>
                @error('datum')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Begintijd</label>
                    <input type="time" class="form-control" name="begintijd" id="begintijd">
                </div>
                @error('begintijd')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Eindtijd</label>
                    <input type="time" class="form-control" name="eindtijd" id="eindtijd">
                </div>
                @error('eindtijd')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">

                <div class="form-group level-up form-group-md">
                    <select name="intern_id" id="intern_id">
                        @foreach(Auth()->user()->Internships->where('status', 'accepted') as $internship )
                            <option  value="{{ $internship->id }}">{{ $internship->Company->company_name }}</option>
                        @endforeach
                    </select>
                </div>

                @error('company_name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Maak nieuw uur aan</span></button>
    </form>
@endsection
