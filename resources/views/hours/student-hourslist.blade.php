@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Urenlijst</h2>
            <a class="btn btn-primary" href="{{ route('hours.show') }}">voeg data toe</a>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Uren</th>
                                <th>datum</th>
                                <th>begintijd</th>
                                <th>eindtijd</th>
                                <th>stage</th>
                                <th>status</th>
                                <th>acties</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($hour as $hours)
                            <tr>
                                <td>{{$hours->hours}}</td>
                                <td>{{explode(' ', $hours->date)[0]}}</td>
                                <td>{{$hours->begintime}}</td>
                                <td>{{$hours->endtime}}</td>
                                <td>{{$hours->internship->Company->company_name}}</td>
                                <td>{{$hours->status}}</td>
                                @if($hours->status == 'Unclear')
                                <td>
                                    <a href="change-hours{{$hours->id}}">
                                        <button class="btn-sm btn-primary">
                                            <ion-icon name="create-outline"></ion-icon>
                                        </button>
                                    </a>
                                    <a href="delete-hours{{$hours->id}}">
                                        <button type="submit" class="btn-sm btn-danger">
                                            <ion-icon name="trash-outline"></ion-icon>
                                        </button>
                                    </a>
                                </td>
                                @else
                                    <td></td>
                                    <td></td>
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                </div>
        </div>
    </div>

@endsection
