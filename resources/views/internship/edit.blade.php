@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Stageplek aanpassen</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1 p-3">
            <form action="/my-company/requests/edit/{{$internship->id}}" method="post" class="mb-0">
                @csrf
                <div class="form-group">
                    <label>Functie</label>
                    <input type="text" class="form-control" placeholder="Zorg assistente"
                           value="{{$internship->function}}" required name="function">
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Van</label>
                            <input type="date" class="form-control" placeholder="dd-mm-yyyy"
                                   value="{{$internship->start_date}}" required name="start_date">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tot</label>
                            <input type="date" class="form-control" placeholder="dd-mm-yyyy"
                                   value="{{$internship->end_date}}" required name="end_date">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uren per week</label>
                            <input type="number" class="form-control" placeholder="40" value="{{$internship->hours}}"
                                   required name="hours">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>
                        @foreach(\App\Models\Internship::$availableStatus as $status)
{{--                            Todo:: translate--}}
                            <option @if($status === $internship->status) selected @endif value="{{$status}}">{{ucfirst(str_replace('_', ' ', $status))}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary">Opslaan</button>
            </form>
        </div>
    </div>

@endsection
