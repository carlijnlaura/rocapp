@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Mijn stagaires</h2>
            @if(\request()->get('f'))
                <a href="/my-company/stagaires" class="btn btn-info mr-3">Huidige</a>
            @else
                <a href="/my-company/stagaires?f=true" class="btn btn-info mr-3">Ontslagen</a>
            @endif
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        @if(count($internships))
            @foreach($internships as $internship)
                <div class="yoo-card yoo-style1 p-3 mt-4 d-flex justify-content-between">
                    <a href="/students/{{$internship->User->id}}">
                        <span class="d-flex justify-content-between">
                            {{$internship->User->getFullName()}} - {{$internship->User->email}}
                        </span>
                        <span class="d-flex justify-content-between">
                            <i class="mr-1">{{$internship->function}}</i> van {{$internship->start_date}}
                            tot {{$internship->end_date}}
                        </span>
                    </a>
                    @if(!\request()->get('f'))
                        <div class="d-flex justify-content-between">
                            <a href="/my-company/requests/edit/{{$internship->id}}"
                               class="btn btn-success mr-3">Bewerk</a>
                        </div>
                    @endif
                </div>
            @endforeach
                @if($internships->hasPages())
                    <div class="yoo-card yoo-style1 p-3 mt-4">
                        {{$internships->links()}}
                    </div>
                @endif
        @else
            <div class="yoo-card yoo-style1 p-3">
                Er zijn geen stagaires in uw bedrijf
            </div>
        @endif
    </div>

@endsection
