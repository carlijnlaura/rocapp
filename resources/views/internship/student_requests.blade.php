@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Aanvragen</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        @if(count($internships))
            @foreach($internships as $internship)
                <div class="yoo-card yoo-style1 p-3 d-flex justify-content-between mt-4">
                    <a href="javascript:void(0)">
                        <span class="d-flex justify-content-between">
                            {{$internship->User->getFullName()}} - {{$internship->User->email}}
                        </span>
                        <span class="d-flex justify-content-between">
                            <i class="mr-1">{{$internship->function}}</i> van {{$internship->start_date}}
                            tot {{$internship->end_date}}
                        </span>
                    </a>
                    <div>
                        {{$internship->status}}
                        <br>
                        {{$internship->Company->company_name}}
                    </div>
                </div>
            @endforeach
        @else
            <div class="yoo-card yoo-style1 p-3">
                Je hebt geen actieve aanvragen
            </div>
        @endif
    </div>

@endsection
