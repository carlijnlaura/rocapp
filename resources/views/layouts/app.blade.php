<html class="no-js hydrated" lang="en" style="visibility: inherit">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{env('APP_NAME')}}</title>
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>

<body class="yoo-white-bg" style="background-color: #f2f2f6">
@include('_partials.header')
@include('_partials.sidebar')

<div class="yoo-height-b60 yoo-height-lg-b60"></div>
<div class="yoo-content yoo-style1">
    <div class="p-3">
        @yield('content')
    </div>
</div>

<script src="{{asset('js/app.js')}}"></script>
<script type="module" src="{{asset('ionicons/dist/ionicons.js')}}"></script>

</body>
</html>
