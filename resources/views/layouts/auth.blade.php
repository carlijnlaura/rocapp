<html class="no-js hydrated" lang="en" style="visibility: inherit">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>

<body class="yoo-white-bg" style="background-color: #f2f2f6">

@yield('content')

<script src="{{asset('js/app.js')}}"></script>
<script type="module" src="{{asset('ionicons/dist/ionicons.js')}}"></script>

</body>
</html>
