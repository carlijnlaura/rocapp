@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading">
            <h2 class="yoo-uikits-title">Settings</h2>
        </div>
    </div>
    <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        <div class="yoo-flex">
                            <ul class="nav nav-tabs yoo-setting-tab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#Profile"><span
                                            class="yoo-tab-icon">
                                            <ion-icon name="person-circle" role="img" class="md hydrated"
                                                      aria-label="person circle"></ion-icon>
                                        </span>Profiel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#Security"><span class="yoo-tab-icon">
                                            <ion-icon name="lock-closed-outline"></ion-icon>
                                        </span>Beveiliging</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#Maps"><span class="yoo-tab-icon">
                                            <ion-icon name="earth-outline"></ion-icon>
                                        </span>Google maps</a>
                                </li>
                            </ul>
                        </div>
                        <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        <hr>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="Profile">
                                <form action="/profile" method="POST">
                                    @csrf
                                    <div class="yoo-profile-setting-container">
                                        <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Profiel foto</label>
                                            <div class="yoo-form-field">
                                                <div class="yoo-setting-profile-pic border-0">
                                                    <img src="{{$user->getAvatar()}}" alt=""
                                                         style="width: 128px; height: 128px" class="rounded">
                                                    <input type="file" name="avatar" id="avatar" class="d-none">
                                                    <button type="button" onclick="$('#avatar').click()"
                                                            class="yoo-profile-pic-edit-btn">
                                                        <ion-icon name="pencil" role="img" class="md hydrated"
                                                                  aria-label="pencil"></ion-icon>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Bedrijfsnaam</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="company_name" class="form-control"
                                                       placeholder="Bedrijfsnaam"
                                                       value="{{$user->company_name}}">
                                            </div>
                                        </div>
                                        @error('company_name')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Telefoon nummer</label>
                                            <div class="input-group form-group-sm">
                                                <input type="number" name="phone_number" class="form-control"
                                                       placeholder="Phone number"
                                                       value="{{$user->phone_number}}">
                                            </div>
                                        </div>
                                        @error('phone_number')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Website</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="website" class="form-control"
                                                       placeholder="Website"
                                                       value="{{$user->website}}">
                                            </div>
                                        </div>
                                        @error('website')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">KVK-Nummer</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="kvk" class="form-control"
                                                       placeholder="KVK-Nummer"
                                                       value="{{$user->kvk}}">
                                            </div>
                                        </div>
                                        @error('website')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Email</label>
                                            <div class="input-group form-group-sm">
                                                <input type="email" name="email" class="form-control"
                                                       placeholder="Email"
                                                       value="{{$user->email}}">
                                            </div>
                                        </div>
                                        @error('email')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b60 yoo-height-lg-b60"></div>
                                    </div>
                                    <hr>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                    <div class="yoo-profile-btn-group yoo-style1">
                                        <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="Maps">
                                <form action="/update-adres" method="POST">
                                    @csrf
                                    <div class="yoo-profile-setting-container">
                                        <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label
                                                class="yoo-form-field-label yoo-type1 text-left justify-content-start">Straat</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="street" class="form-control"
                                                       placeholder="Straat"
                                                       value="{{$user->street}}">
                                            </div>
                                        </div>
                                        @error('street')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label
                                                class="yoo-form-field-label yoo-type1 text-left justify-content-start">Postcode</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="zipcode" class="form-control"
                                                       placeholder="Postcode"
                                                       value="{{$user->zipcode}}">
                                            </div>
                                        </div>
                                        @error('zipcode')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label
                                                class="yoo-form-field-label yoo-type1 text-left justify-content-start">Plaats</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="place" class="form-control"
                                                       placeholder="Plaats"
                                                       value="{{$user->place}}">
                                            </div>
                                        </div>
                                        @error('place')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <iframe width="100%" style="margin-top: 20px" height="476px" frameborder="0"
                                                scrolling="no" marginheight="0" marginwidth="0" class="rounded"
                                                src="https://maps.google.com/maps/embed/v1/place?key=AIzaSyD4f8KjyHs9GWJBX6VH_lQIj2hZzImkYRU&q={{$user->street}}+{{$user->zipcode}}+{{$user->place}}"></iframe>
                                        <button style="margin-top: 20px;" type="button" id="refresh-maps"
                                                class="yoo-form-btn yoo-style1 yoo-color3"><span>Refresh maps</span>
                                        </button>
                                    </div>
                                    <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                    <hr>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                    <div class="yoo-profile-btn-group yoo-style1">
                                        <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                                        </button>
                                    </div>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="Security">
                                <form action="/update-password" method="POST">
                                    @csrf
                                    <div class="yoo-profile-setting-container">
                                        <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Huidig wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="old_password" class="form-control"
                                                       placeholder="*******">
                                            </div>
                                        </div>
                                        @error('old_password')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Nieuw wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="new_password" class="form-control"
                                                       placeholder="*******"></div>
                                        </div>
                                        @error('new_password')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Bevestig nieuw
                                                wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="new_password_confirmation"
                                                       class="form-control" placeholder="*******">
                                            </div>
                                        </div>
                                        @error('new_password_confirmation')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b40 yoo-height-lg-b40"></div>
                                        @error('message')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-success">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <hr>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                    <div class="yoo-profile-btn-group yoo-style1">
                                        <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .col -->
        </div>
    </div>
@endsection
