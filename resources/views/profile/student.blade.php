@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading">
            <h2 class="yoo-uikits-title">Settings</h2>
        </div>
    </div>
    <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="yoo-card yoo-style1">
                    <div class="yoo-card-body">
                        <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        <div class="yoo-flex">
                            <ul class="nav nav-tabs yoo-setting-tab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#Profile"><span
                                            class="yoo-tab-icon">
                                            <ion-icon name="person-circle" role="img" class="md hydrated"
                                                      aria-label="person circle"></ion-icon>
                                        </span>Profiel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#Security"><span class="yoo-tab-icon">
                                            <ion-icon name="lock-closed-outline"></ion-icon>
                                        </span>Beveiliging</a>
                                </li>
                            </ul>
                        </div>
                        <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        <hr>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="Profile">
                                <form action="/profile" method="POST">
                                    @csrf
                                    <div class="yoo-profile-setting-container">
                                        <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Profiel foto</label>
                                            <div class="yoo-form-field">
                                                <div class="yoo-setting-profile-pic border-0">
                                                    <img src="{{$user->getAvatar()}}" alt="" style="width: 128px; height: 128px" class="rounded">
                                                    <input type="file" name="avatar" id="avatar" class="d-none">
                                                    <button type="button" onclick="$('#avatar').click()" class="yoo-profile-pic-edit-btn">
                                                        <ion-icon name="pencil" role="img" class="md hydrated"
                                                                  aria-label="pencil"></ion-icon>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Voor naam</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="first_name" class="form-control"
                                                       placeholder="Voor Naam"
                                                       value="{{$user->first_name}}">
                                            </div>
                                        </div>
                                        @error('first_name')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Last Name</label>
                                            <div class="input-group form-group-sm">
                                                <input type="text" name="last_name" class="form-control"
                                                       placeholder="Last Name"
                                                       value="{{$user->last_name}}">
                                            </div>
                                        </div>
                                        @error('last_name')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Email</label>
                                            <div class="input-group form-group-sm">
                                                <input type="email" name="email" class="form-control"
                                                       placeholder="Email"
                                                       value="{{$user->email}}">
                                            </div>
                                        </div>
                                        @error('email')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b60 yoo-height-lg-b60"></div>
                                    </div>
                                    <hr>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                    <div class="yoo-profile-btn-group yoo-style1">
                                        <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="Security">
                                <form action="/update-password" method="POST">
                                    @csrf
                                    <div class="yoo-profile-setting-container">
                                        <div class="yoo-height-b45 yoo-height-lg-b45"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Huidig wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="old_password" class="form-control"
                                                       placeholder="*******">
                                            </div>
                                        </div>
                                        @error('old_password')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Nieuw wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="new_password" class="form-control"
                                                       placeholder="*******"></div>
                                        </div>
                                        @error('new_password')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="yoo-form-field-wrap yoo-style1">
                                            <label class="yoo-form-field-label yoo-type1">Bevestig nieuw
                                                wachtwoord</label>
                                            <div class="input-group form-group-sm">
                                                <input type="password" name="new_password_confirmation"
                                                       class="form-control" placeholder="*******">
                                            </div>
                                        </div>
                                        @error('new_password_confirmation')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                        @enderror
                                        <div class="yoo-height-b40 yoo-height-lg-b40"></div>
                                        @error('message')
                                        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                                        <div class="alert alert-success">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <hr>
                                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                                    <div class="yoo-profile-btn-group yoo-style1">
                                        <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .col -->
        </div>
    </div>
@endsection
