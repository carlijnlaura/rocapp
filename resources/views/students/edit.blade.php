@extends('layouts.app')

@section('content')
    <form action="/students/{{$student->id}}" method="POST">
        @csrf
        @method('put')
        <input type="hidden" name="type" value="student" id="register_type">
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="first_name">Voornaam</label>
                    <input type="text" class="form-control" name="first_name" value="{{$student->first_name}}" id="first_name">
                </div>
                @error('first_name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="last_name">Achternaam</label>
                    <input type="text" class="form-control" name="last_name" value="{{$student->last_name}}" id="last_name">
                </div>
                @error('last_name')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="{{$student->email}}" id="email">
                </div>
                @error('email')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="phone_number">Telefoon nummer</label>
                    <input type="number" class="form-control" name="phone_number" value="{{$student->phone_number}}" id="phone_number">
                </div>
                @error('phone_number')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <button type="submit" class="yoo-form-btn yoo-style1 yoo-color1"><span>Opslaan</span></button>
    </form>
@endsection
