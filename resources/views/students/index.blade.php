@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Studenten</h2>
            <form action="/students" method="get" class="mb-0">
                <div class="form-group level-up mb-0">
                    <label for="exampleInputEmail18">Zoek studenten</label>
                    <input type="search" class="form-control" id="exampleInputEmail18" name="q">
                </div>
            </form>
            @if(\request()->has('q'))
                <a href="/students" class="btn btn-outline-primary">Reset resultaaten</a>
            @endif
            @if(Auth::user()->type == "teacher")
            <a class="btn btn-primary" href="/students/create">Voeg een student toe</a>
                @endif
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
            @if(count($users))
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Voornaam</th>
                                <th>Achternaam</th>
                                <th>Emailadres</th>
                                @if(Auth::user()->type == "teacher")
                                <th width="280px" class="text-right">Acties</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-right">
                                        @if(Auth::user()->type == "teacher")

                                        <a class="btn-sm btn-info mr-1" href="/students/{{$user->id}}">
                                            <ion-icon class="text-white" name="eye-outline"></ion-icon>
                                        </a>

                                        <a class="btn-sm btn-primary mr-1" href="/students/{{$user->id}}/edit">
                                            <ion-icon name="create-outline"></ion-icon>
                                        </a>
                                        <a class="btn-sm btn-danger mr-1" href="/students/{{$user->id}}/delete">
                                            <ion-icon name="trash-outline"></ion-icon>
                                        </a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                </div>
            @else
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Geen studenten gevonden
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
