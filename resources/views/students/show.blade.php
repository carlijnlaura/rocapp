@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Student informatie</h2>
        </div>

        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1 ">
            <div class="yoo-padd-lr-30">
                <div class="yoo-height-b30 yoo-height-lg-b30"></div>
                <div class="yoo-user yoo-style4">
                    <div class="yoo-user-img">
                        <img src="{{$student->getAvatar()}}" alt="">
                    </div>
                    <div class="yoo-user-info">
                        <h3 class="yoo-user-name">{{$student->getFullName()}}</h3>
                        @foreach($student->Internships()->where('status', 'accepted')->get() as $internship)
                            <span class="yoo-lavel">{{$internship->function}} @ {{$internship->Company->company_name}}</span><br>
                        @endforeach
                        <div class="yoo-height-b15 yoo-height-lg-b15"></div>
                        <div class="yoo-user-btns">
                            {{--                            <a href="#" class="btn btn-outline-link">Message</a>--}}
                            {{--                            <button class="yoo-btn yoo-toolbtn dropdown-toggle yoo-table-btn2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><ion-icon name="ellipsis-horizontal" role="img" class="md hydrated" aria-label="ellipsis horizontal"></ion-icon></button>--}}
                            {{--                            <div class="dropdown-menu">--}}
                            {{--                                <a class="dropdown-item" href="#">All</a>--}}
                            {{--                                <a class="dropdown-item" href="#">Read</a>--}}
                            {{--                                <a class="dropdown-item" href="#">Unread</a>--}}
                            {{--                                <a class="dropdown-item" href="#">Starred</a>--}}
                            {{--                                <a class="dropdown-item" href="#">Unstarred</a>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="yoo-height-b30 yoo-height-lg-b30"></div>
                <hr>
                <div class="yoo-height-b25 yoo-height-lg-b25"></div>
                <ul class="yoo-contact-info-list yoo-mp0">
                    <li>
                        <div class="yoo-contact-info-label">Email</div>
                        <div class="yoo-contact-info-details">{{$student->email}}</div>
                    </li>
                    <li>
                        <div class="yoo-contact-info-label">Contact Nummer</div>
                        <div
                            class="yoo-contact-info-details">{{$student->phone_number === '' ? 'Niet beschikbaar' : ''}}</div>
                    </li>
                    {{--                    <li>--}}
                    {{--                        <div class="yoo-contact-info-label">Location</div>--}}
                    {{--                        <div class="yoo-contact-info-details">London, United Kingom</div>--}}
                    {{--                    </li>--}}
                    {{--                    <li>--}}
                    {{--                        <div class="yoo-contact-info-label">Timezone</div>--}}
                    {{--                        <div class="yoo-contact-info-details">7 PM local time</div>--}}
                    {{--                    </li>--}}
                </ul>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
            </div>
        </div>
        <div class="yoo-card yoo-style1 mt-4">
            <div class="yoo-padd-lr-30">
                <div class="yoo-height-b30 yoo-height-lg-b30"></div>
                @foreach($student->trainings as $training)

                    <td>{{$training->title}}</td>
                @endforeach
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
            </div>
        </div>
    </div>

@endsection
