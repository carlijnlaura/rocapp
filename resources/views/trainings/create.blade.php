@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading">
            <h2 class="yoo-uikits-title">Nieuwe opleiding</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>
    <form method="POST" action="{{route('trainings.store')}}">
        @csrf
        <div class="container">
            <div class="yoo-card yoo-style1">
                <div class="yoo-card-body">
                    <div class="row">
                        <div class="col-11">
                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>


                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Titel</label>
                                <div class="input-group form-group-sm">
                                    <input type="text" name="title" class="form-control" value="{{old('title')}}">
                                </div>
                            </div>

                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Niveau</label>
                                <div class="input-group form-group-sm">
                                    <input type="number" name="niveau" class="form-control" value="{{old('niveau')}}">
                                </div>
                            </div>
                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Sector</label>
                                <div class="input-group form-group-sm">
                                    <input type="text" name="sector" class="form-control" value="{{old('sector')}}">
                                </div>
                            </div>
                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Leerweg</label>
                                <div class="yoo-select">
                                    <select class="form-control" id="exampleFormControlSelect1" name="leerweg">
                                        <option value="BOL" {{ old('leerweg') == 'BOL' ? 'selected' : '' }}>
                                            BOL
                                        </option>
                                        <option value="BBL" {{ old('leerweg') == 'BBL' ? 'selected' : '' }}>
                                            BBL
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Duur</label>
                                <div class="input-group form-group-sm">
                                    <input type="number" name="duration" class="form-control"
                                           value="{{old('duration')}}">
                                </div>
                            </div>
                            <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                            <div class="yoo-form-field-wrap yoo-style1">
                                <label class="yoo-form-field-label yoo-type1">Beschrijving</label>
                                <div class="input-group form-group-sm">
                            <textarea class="form-control" name="description" cols="30"
                                      rows="4">{{old('description')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                </div>
                @if(count($errors->all()))
                    @foreach ($errors->all() as $error)
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="alert alert-danger mt-2">
                                    {{$error}}
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                    @endforeach
                @endif
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                    <button type="submit" class="yoo-profile-btn yoo-style1 yoo-color1">Opslaan
                    </button>
                </div>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
            </div>
        </div>
    </form>

@endsection
