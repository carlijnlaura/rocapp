@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Opleidingen</h2>
        @if(Auth::user()->type == "teacher")
            <a class="btn btn-primary" href="{{ route('trainings.create') }}">Voeg een opleiding toe</a>
            @endif
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
            @if(count($trainings))
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <form action="{{ route('trainings.index') }}" method="GET" role="search">

                            <div class="input-group">
                                <input type="text" class="form-control mr-2 rounded" name="title" placeholder="Zoek opleiding" id="title">
                                <a href="{{ route('trainings.index') }}" class=" mt-1">
                                </a>

                            </div>

                        </form>

                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Titel</th>
                                <th>Duur</th>
                                <th>Niveau</th>
                                <th>Sector</th>
                                <th>Leerweg</th>
                                <th width="280px" class="text-right">Acties</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($trainings as $training)
                                <tr>
                                    <td>{{ $training->title }}</td>
                                    <td>{{ $training->duration }}</td>
                                    <td>{{ $training->niveau }}</td>
                                    <td>{{ $training->sector }}</td>
                                    <td>{{ $training->leerweg }}</td>
                                    <td class="text-right">
                                        <a href="/trainings/{{$training->id}}" class="btn btn-sm btn-info">
                                            <ion-icon class="text-white" name="eye-outline"></ion-icon>
                                        </a>


                                        <form action="/send-mail"
                                              style="display: unset"
                                              method="POST">
                                            @csrf

                                            <input type="hidden"
                                                   id="training"
                                                   name="training"
                                                   value="{{ $training->id }}"/>
                                            @if(Auth::user()->type == "student")
                                            <button type="submit" class="btn-sm btn-blue-gray btn-disabled" onclick="return confirm('Weet u zeker dat u deze email wilt sturen?')">
                                                <ion-icon class="text-white" name="mail-outline"></ion-icon>
                                            </button>
                                                @endif

                                        </form>
                                        @if(Auth::user()->type == "teacher")
                                            <button class="btn-sm btn-primary"

                                                    onclick="window.location.href = '{{ route('trainings.edit',$training->id) }}'">
                                            <ion-icon name="create-outline"></ion-icon>
                                        </button>

                                        <form action="{{ route('trainings.destroy',$training->id) }}"
                                              style="display: unset"
                                              method="POST">
                                            <button type="submit" class="btn-sm btn-danger">
                                                <ion-icon name="trash-outline"></ion-icon>
                                            </button>
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                    {!! $trainings->links() !!}
                </div>
            @else
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Er zijn geen opleidingen gevonden.
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
