@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">

            <h2 class="yoo-uikits-title">Opleiding {{$training->title}} </h2>
        </div>
        @if(Auth::user()->type == "teacher")
        <a class="btn btn-primary" href="{{ route('trainings.student.index', [$training->id]) }}">Voeg een student toe</a>
        @endif
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1 ">
            <table class="table">
                <thead>
                <tr>
                    <th class="text-center pt-3"><h5>Opleiding informatie</h5></th>
                </tr>
                </thead>
            </table>

            <table>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Naam: {{$training->title}}  </h6></td>
                </tr>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Duur: {{$training->duration}}  </h6></td>
                </tr>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Niveau: {{$training->niveau}}  </h6></td>
                </tr>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Sector: {{$training->sector}}  </h6></td>
                </tr>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Leerweg: {{$training->leerweg}}  </h6></td>
                </tr>
                <tr>
                    <td class="px-5"> <h6 class="font-weight-light"> Beschrijving: {{$training->description}}  </h6></td>
                </tr>

            </table>
        </div>
    </div>

@endsection
