@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="yoo-uikits-heading d-flex justify-content-between">
            <h2 class="yoo-uikits-title">Studenten</h2>
        </div>
        <div class="yoo-height-b30 yoo-height-lg-b30"></div>
    </div>

    <div class="container">
        <div class="yoo-card yoo-style1">
            @if(count($users))
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Voornaam</th>
                                <th>Achternaam</th>
                                <th>Emailadres</th>
                                <th width="280px" class="text-right">Acties</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <form action="{{route('trainings.student.store', [$training->id])}}" method="post" id="">
                                    @csrf
                                <tr>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-right">
                                        <input type="hidden" id="id" name="user_id"
                                               value="{{$user->id}}">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            <ion-icon name="checkmark-outline"></ion-icon>
                                        </button>

                                    </td>
                                </form>

                                @endforeach

                                </tr>
                            </tbody>

                        </table>
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                    </div>
                </div>
                <hr>
                <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                <div class="yoo-profile-btn-group yoo-style1">
                </div>
            @else
                <div class="yoo-card-body">
                    <div class="yoo-padd-lr-20">
                        <div class="yoo-height-b20 yoo-height-lg-b20"></div>
                        Geen studenten gevonden
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
