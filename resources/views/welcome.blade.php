<html class="no-js hydrated slzcqdxndx idc0_326" lang="en">
<head>
    <meta charset="utf-8">
    <style data-styles="">
        .hydrated {
            visibility: inherit
        }</style>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>

<body class="yoo-white-bg" style="background-color: #f2f2f6">
<div class="yoo-login-wrap yoo-style3 yoo-bg">
    <form action="#" class="yoo-form yoo-style2 yoo-border">
        <div class="yoo-form-logo">
            <img style="height: 70px;" src="https://www.rocvanflevoland.nl/assets/img/og-logo-rocvf.png" alt="">
        </div>
        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
        <h2 class="yoo-form-title">Log in om verder te gaan</h2>
        <div class="yoo-form-subtitle">Nog geen account? <a href="#" class="yoo-form-btn yoo-style2">Registreer
                hier</a></div>
        <div class="yoo-height-b25 yoo-height-lg-b25"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group level-up form-group-md">
                    <label for="password">Wachtwoord</label>
                    <input type="password" class="form-control" id="password">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="yoo-forget-pass-wrap">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="gridCheck">
                            <label class="custom-control-label" for="gridCheck">
                                <span class="custom-control-shadow"></span>Onthoud mij
                            </label>
                        </div>
                        <a href="#" class="yoo-form-btn yoo-style2">Wachtwoord vergeten?</a>
                    </div>
                </div>
                <a href="#" class="yoo-form-btn yoo-style1 yoo-color1"><span>Log in</span></a>
            </div>
        </div>
    </form>
</div>

<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
