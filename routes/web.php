<?php

use App\Http\Controllers\StudentController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\TrainingStudentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HourController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\KDController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\InternshipController;
use App\Http\Controllers\InternshiphoursController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['middleware' => ['auth']], function () {

    Route::resource('students', StudentController::class)->only(['index']); //users without the role teacher can only see the INDEX view

    Route::group(['middleware' => ['teacher']], function () { //users with the teacher role have acces to
        Route::resource('trainings', TrainingController::class)->except(['index','show']); //complete resource of trianings
        Route::resource('students', StudentController::class)->except(['index']); //complete resource of student

    });
    Route::resource('trainings', TrainingController::class)->only(['index','show']); //users without the role teacher can only see the INDEX view

    Route::resources([
        'hours' => HourController::class,
        'trainings.student' => TrainingStudentController::class,
        'internship_hours' => \App\Http\Controllers\InternshiphoursController::class,
        'teachers' => TeacherController::class,
        'companies' => \App\Http\Controllers\CompanyController::class,
        'KD' => KDController::class,
    ]);
    Route::get('school-company', [UserController::class, 'index'])->name('school.company');

    Route::get('show-hours', [HourController::class, 'show'])->name('hours.show');

    Route::get('/company/{company}/request', [InternshipController::class, 'request']);
    Route::get('/company/requests', [InternshipController::class, 'studentsRequests']);
    Route::post('/company/{company}/request', [InternshipController::class, 'requestPost']);

    Route::group(['prefix' => 'my-company'], function () {
        Route::get('requests', [InternshipController::class, 'requests']);
        //Todo:: improve these routes
        Route::get('requests/edit/{internship}', [InternshipController::class, 'edit']);
        Route::post('requests/edit/{internship}', [InternshipController::class, 'update']);
        Route::get('requests/deny/{internship}', [InternshipController::class, 'denyRequest']);
        Route::get('requests/accept/{internship}', [InternshipController::class, 'acceptRequest']);

        Route::get('stagaires', [InternshipController::class, 'stagaires']);
    });


    Route::get('create-company', [TeacherController::class, 'createcompanypage']);
    Route::get('change-company', [TeacherController::class, 'changecompanypage']);
    Route::get('change-hours{id}', [HourController::class, 'edit'])->name('change.hours');
    Route::get('delete-hours{id}', [HourController::class, 'destroy'])->name('delete.hours');
    Route::get('good-hours{id}', [InternshiphoursController::class, 'create'])->name('good.hours');
    Route::get('bad-hours{id}', [HourController::class, 'deny'])->name('bad.hours');
    Route::get('hours.update{id}', [HourController::class, 'update'])->name('hours.update');
    Route::get('created-company', [TeacherController::class, 'createcompany'])->name('create.company');
    Route::get('/changed-company{id}', [TeacherController::class, 'changecompany'])->name('change.company');
    Route::get('/delete-company/{id}', [TeacherController::class, 'deletecompany'])->name('delete.company');

    Route::get('/students/{student}/delete', [StudentController::class, 'delete']);
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/private/{file?}', 'FileController@get')->where('file', '(.*)');

    Route::get('/KD.showStudentKD', [App\Http\Controllers\KDController::class, 'showStudentKD']);

    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'show']);
    Route::post('/profile', [App\Http\Controllers\ProfileController::class, 'update']);

    Route::post('/update-password', [App\Http\Controllers\ProfileController::class, 'updatePassword']);
    Route::post('/send-mail', [App\Http\Controllers\MailController::class, 'store']);
    Route::post('/update-avatar', [App\Http\Controllers\ProfileController::class, 'updateAvatar']);
    Route::post('/update-adres', [App\Http\Controllers\ProfileController::class, 'updateAdres']);
});






